CC=clang++
CXXFLAGS=-O3 -std=c++23 -ljsoncpp -lpthread
SHELL=/bin/bash

build-main: main.cpp

	$(CC) $(CXXFLAGS) -o build/main src/main.cpp
	 