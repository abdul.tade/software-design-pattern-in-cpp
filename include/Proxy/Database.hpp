# pragma once

# include <iostream>
# include <mutex>
# include <vector>
# include "DbInterface.hpp"

/**
 * @brief Class that implements the database of log messages 
 * ensure that output filestream is opened in append mode
*/
class Database : public DbInterface 
{
    private:
        std::ostream& outStream;
        std::mutex mutex{};

    public:

        Database(std::ostream& outStream)
            : outStream(outStream)
            {}
        
        void insert(std::string const& message) override {
            std::lock_guard<std::mutex> lock(mutex);
            outStream.write(message.c_str(),message.size());
        }

        void insertMany(std::vector<std::string>& messages) override {
            std::lock_guard<std::mutex> lock(mutex);
            for (auto const& e : messages) {
                auto m = e + "\n";
                outStream.write(m.c_str(),e.size());
            }
        }

};