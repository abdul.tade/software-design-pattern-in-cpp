# pragma once
# include <iostream>
# include <vector>


class DbInterface {
    virtual void insert(std::string const& message) = 0;
    virtual void insertMany(std::vector<std::string>& messages) = 0;
};