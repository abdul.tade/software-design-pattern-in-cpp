# pragma once
# include <iostream>
# include <mutex>
# include "Database.hpp"
# include "DbInterface.hpp"


class CachingProxy : public DbInterface {

    private:

        Database db;
        std::mutex mutex;
        std::string aggregateString = "";
        const std::size_t maxInsertionCount = 10;
        std::size_t insertionCount = 0;
        std::vector<std::string> v{maxInsertionCount};

    public:

        CachingProxy(std::ostream& outStream)
            : db(Database{outStream})
            {}

        void insert(std::string const& message) override {
            std::lock_guard<std::mutex> lock(mutex);

            if (insertionCount == maxInsertionCount) {
                aggregateString += message;
                db.insert(aggregateString);
                aggregateString = "";
                insertionCount = 0;
            }

            aggregateString += message;
            ++insertionCount;
        }

        void insertMany(std::vector<std::string>& messages)
        {
            for (auto const& message : messages) {
                this->insert(message);
            }
        }
};