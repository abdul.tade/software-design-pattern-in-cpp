# pragma once
# include <iostream>
# include <algorithm>

class JsonDataReader
{
    private:

    public:

        JsonDataReader() {}

        virtual std::string readJSON(std::string jsonData) const {
            std::transform(jsonData.begin(),jsonData.end(),jsonData.begin(),[](char c) {
                return (char)(std::tolower((int)c));
            });
            return "[JSON] " + jsonData;
        }

        virtual ~JsonDataReader() {}
};

