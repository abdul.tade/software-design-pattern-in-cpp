# pragma once
# include "JsonDataReader.hpp"

class Client 
{
    private:
        JsonDataReader* jsonDataReader;
    public:

        Client(JsonDataReader* jsonDataReader)
            : jsonDataReader(jsonDataReader)
        {}

        void printJsonToConsole(std::string data) {
            std::cout << jsonDataReader->readJSON(data) << std::endl;
        }
};