# pragma once
# include <iostream>
# include <algorithm>

class XmlDataReader
{

    public:

        XmlDataReader() {}

        virtual std::string readXML(std::string xmlData) const {
            std::transform(xmlData.begin(),xmlData.end(),xmlData.begin(),[](char c) {
                return (char)std::toupper((int)c);
            });
            return "[XML] " + xmlData;
        }
    
        virtual ~XmlDataReader() {}

};