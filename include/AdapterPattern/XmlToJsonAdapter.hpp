# pragma once
# include <iostream>
# include <sstream>
# include <vector>
# include <numeric>
# include "XmlDataReader.hpp"
# include "JsonDataReader.hpp"

class XmlToJsonAdapter : public JsonDataReader
{
    private:
        XmlDataReader* xmlDataReader;
    public:

        XmlToJsonAdapter(XmlDataReader* xmlDataReader)
            : xmlDataReader(xmlDataReader)
        {}

        std::string readJSON(std::string xmlData) const override {

            auto Data = xmlDataReader->readXML(xmlData);
            std::vector<std::string> tokens;
            std::string token;
            std::stringstream ss{Data};

            while (std::getline(ss,token,' ')) {
                tokens.push_back(token);
            }

            tokens[0] = std::string("[JSON]");
            auto s = std::accumulate(tokens.begin(),tokens.end(),std::string(""));

            return s;
        }
    
        virtual ~XmlToJsonAdapter() noexcept {}
};