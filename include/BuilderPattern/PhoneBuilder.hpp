# pragma once
# include <string>
# include <memory>
# include "Phone.hpp"

class PhoneBuilder
{
    private:
        std::shared_ptr<Phone> phone{};

    public:

        PhoneBuilder() {
            phone = std::shared_ptr<Phone>(new Phone);
        }

        auto set_name(std::string const& name) {
            phone->name = name;
            return *this;
        }

        auto set_model(std::string const& model) {
            phone->model = model;
            return *this;
        }

        auto set_version(std::string const& version) {
            phone->version = version;
            return *this;
        }

        auto set_cpu_type(std::string const& cpu_type) {
            phone->cpu_type = cpu_type;
            return *this;
        }

        auto set_phone_id(std::string const& phone_id) {
            phone->phone_id = phone_id;
            return *this;
        }

        std::shared_ptr<Phone> get_phone() {
            return phone;
        }

        virtual ~PhoneBuilder() noexcept {

        }
};