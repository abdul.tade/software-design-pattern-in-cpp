# pragma once

# include <string>

struct Phone {
    public:

        std::string name;
        std::string model;
        std::string version;
        std::string cpu_type;
        std::string phone_id;
    
        virtual ~Phone() noexcept {

        }
};