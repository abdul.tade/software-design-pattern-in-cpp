# pragma once
# include <iostream>

struct Product {

    public:
    
        Product() = default;

        Product(Product const& product) 
            : name(product.name),
              description(product.description),
              img_url(product.img_url),
              price(product.price),
              quantity(product.quantity)
        {}

        std::string name;
        std::string description;
        std::string img_url;
        double price;
        std::size_t quantity;

        friend std::ostream& operator<<(std::ostream& oss,Product const& product) {
            oss << "Product {\n\t" 
                << "name: "            << product.name
                << "\n\tdescription: " << product.description
                << "\n\timg_url: "     << product.img_url
                << "\n\tprice: USD"    << product.price
                << "\n\tquantity: "    << product.quantity
                << "\n}";
            return oss;
        }

        virtual ~Product() {

        }
};