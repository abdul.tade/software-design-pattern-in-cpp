# pragma once
# include <iostream>
# include <queue>
# include <mutex>
# include "EmptyQueueException.hpp"

template <class ProductType>
class ThreadSafeQueue
{
    private:

        std::mutex mutex{};
        std::queue<ProductType> product_queue{};

    public:

        ThreadSafeQueue() = default;
        ThreadSafeQueue(std::initializer_list<ProductType> list) {
            for (auto const& p : list) {
                product_queue.push(p);
            }
        }

        ThreadSafeQueue(ThreadSafeQueue const& q) = delete;
        ThreadSafeQueue(ThreadSafeQueue&& q) = delete;
        ThreadSafeQueue<ProductType>& operator=(ThreadSafeQueue<ProductType> const& q) = delete;

        void enqueue(ProductType const& product) {
            std::lock_guard<std::mutex> lock(mutex);
            product_queue.push(product);
        }

        ProductType dequeue() {
            std::lock_guard<std::mutex> lock(mutex);
            if (product_queue.empty()) {
                throw EmptyQueueException{};
            }
            
            auto product = product_queue.front();
            product_queue.pop();
            return product;
        }

        std::size_t size() {
            std::lock_guard<std::mutex> lock(mutex);
            return product_queue.size();
        }

        bool is_empty() {
            std::lock_guard<std::mutex> lock(mutex);
            return product_queue.empty();
        }
};