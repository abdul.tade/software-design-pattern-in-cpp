# pragma once
# include <exception>

class EmptyQueueException : public std::exception
{
    private:
        const char* message;
    public:

        EmptyQueueException()
            : message("Queue is empty")
        {}

        EmptyQueueException(const char* message)
            : message(message)
        {}

        const char* what() {
            return message;
        }
};