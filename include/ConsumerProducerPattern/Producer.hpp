# pragma once
# include <iostream>
# include <thread>
# include <mutex>
# include <queue>
# include <chrono>
# include <future>
# include "../LoggingService/LoggingService.hpp"
# include "ThreadSafeQueue.hpp"
# include "Product.hpp"

template <class ProductType>
class Producer 
{
    private:
        
        std::string name;
        LoggingInterface& logger;
        ThreadSafeQueue<ProductType>& product_queue;

        Product create_product() {
            Product product;

            product.name = "Some name";
            product.description = " Some description";
            product.img_url = "some/path/to/image";
            product.price = 10.343;
            product.quantity = 100;

            return product;
        }

        std::string get_producer_id() {
            return "[" + name + "]";
        }

    public:
        
        Producer(std::string const& name,ThreadSafeQueue<ProductType>& product_queue,LoggingInterface& logger)
            : name(name),
              product_queue(product_queue),
              logger(logger)
            {}

        auto operator()(int const& seconds) {
            std::this_thread::sleep_for(std::chrono::seconds(seconds));

            auto f = [&]() mutable {

                while (true) {

                    auto p = create_product();
                    product_queue.enqueue(p);

                    logger.log(get_producer_id(),"Item has been added to queue");
                    logger.log(get_producer_id(),"Current queue size " + std::to_string(product_queue.size()));

                    std::this_thread::sleep_for(std::chrono::seconds(seconds));
                }
 
            };

            auto async_object = std::async(std::launch::async,f);
            return async_object;
        }
};