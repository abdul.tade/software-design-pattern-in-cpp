# pragma once
# include <iostream>
# include "../Proxy/CachingProxy.hpp"

class LoggingService {

    private:
        CachingProxy proxy;

    public:

        LoggingService(std::ostream& outStream)
            : proxy(CachingProxy{outStream})
            {}
        
        void log(std::string const& message) {
            proxy.insert(message);
        }
};