# pragma once
# include <iostream>
# include <mutex>
# include <thread>
# include <queue>
# include <chrono>
# include <future>
# include "ThreadSafeQueue.hpp"
# include "Product.hpp"

template <typename ProductType>
class Consumer
{
    private: 

        std::string name;
        LoggingInterface& logger;
        ThreadSafeQueue<ProductType>& product_queue;

        std::string get_consumer_id() {
            return "[" + name + "]";
        }

    public:

        Consumer() = delete;
        Consumer(Consumer const& customer) = delete;
        Consumer(Consumer&& customer) = delete;

        Consumer(std::string const& name,ThreadSafeQueue<ProductType>& product_queue,LoggingInterface& logger)
            : name(name),
              product_queue(product_queue),
              logger(logger)
        {}

        std::future<void> operator()(int const& seconds)() {

            auto f = [&]() mutable {

                while (true) 
                {

                    if (product_queue.is_empty()) 
                    {
                        logger.log(get_consumer_id(),"Queue is empty");
                    } else 
                    {
                        ProductType p = product_queue.dequeue();
                        logger.log(get_consumer_id(),"Item has been consumed");
                    }
                }
            };

        auto async_object = std::async(std::launch::async,f);
        return async_object;
}

};


