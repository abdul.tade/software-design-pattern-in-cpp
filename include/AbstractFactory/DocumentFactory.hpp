# pragma once
# include <iostream>
# include <vector>
# include <memory>
# include "AbstractFileFactory.hpp"
# include "File.hpp"
# include "TextFile.hpp"
# include "BinaryFile.hpp"
# include "YmlDocument.hpp"
# include "PortableDocumentFormat.hpp"

class DocumentFactory : public AbstractFileFactory
{
    private:
        TextFile* textFile = nullptr;
        BinaryFile* binaryFile = nullptr;

    public:

        DocumentFactory() = default;

        TextFile* createTextFile(std::string const& filename) {
            textFile = new YmlDocument(filename);
            return textFile;
        }

        BinaryFile* createBinaryFile(std::string const& filename) {
            binaryFile = new PortableDocumentFormat(filename);
            return binaryFile;
        }

       virtual ~DocumentFactory() {

            if (textFile != nullptr) {
                delete textFile;
                textFile = nullptr;
            }

            if (binaryFile != nullptr) {
                delete binaryFile;
                binaryFile = nullptr;
            }

        }

};