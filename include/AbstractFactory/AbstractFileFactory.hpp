# pragma once
# include <iostream>
# include "BinaryFile.hpp"
# include "TextFile.hpp"

class AbstractFileFactory
{
    public:
    
        AbstractFileFactory() = default;

        virtual BinaryFile* createBinaryFile(std::string const& filename) = 0;
        virtual TextFile*   createTextFile(std::string const& filename) = 0;

        virtual ~AbstractFileFactory() {

        }
};