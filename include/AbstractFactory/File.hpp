# pragma once
# include <iostream>
# include <vector>

class File 
{
    public:
        virtual void readFile() = 0;
        virtual void writeFile() = 0;
        virtual void getEncoding() = 0;
        virtual void parseFile() = 0;
        virtual ~File() {

        }
};