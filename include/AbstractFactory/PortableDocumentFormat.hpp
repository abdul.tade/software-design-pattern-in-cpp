# pragma once
# include <iostream>
# include "BinaryFile.hpp"

class PortableDocumentFormat : public BinaryFile
{
    private:
        std::string filename;

    public:

        PortableDocumentFormat(std::string const& filename) 
            : BinaryFile(filename) 
        {}

        void readFile() override {
            std::cout << "reading a PDF file from " << filename << std::endl;
        }

        void writeFile() override {
            std::cout << "writing file to PDF file " << filename << std::endl;
        }

        void parseFile() override {
            std::cout << "Parsing PDF file" << std::endl;
        }

        virtual ~PortableDocumentFormat() {

        }
};