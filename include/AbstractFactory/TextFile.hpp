# pragma once
# include <iostream>
# include <vector>
# include "File.hpp"

class TextFile : public File
{
    private:
        std::string filename;

    public:

        TextFile(std::string const& filename) 
            : filename(filename)
        {}

        void getEncoding() override {
            std::cout << "ascii encoding" << std::endl;
        }

        virtual ~TextFile() {
        }
};