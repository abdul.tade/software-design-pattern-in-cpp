# pragma once
# include <iostream>
# include "TextFile.hpp"

class YmlDocument : public TextFile
{

    private:

        std::string filename;

    public:

        YmlDocument(std::string const& filename) 
            : TextFile(filename) 
        {}

        void readFile() override {
            std::cout << "reading yml document from " << filename << std::endl;
        }

        void writeFile() override {
            std::cout << "writing yml document to "   << filename << std::endl;
        }

        void parseFile() override {
            std::cout << "parsing a YML Document" << filename << std::endl;
        }

        virtual ~YmlDocument() {

        }

};