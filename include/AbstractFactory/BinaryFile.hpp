# pragma once
# include <iostream>
# include <vector>
# include "File.hpp"

class BinaryFile : public File
{
    private:
        std::string filename;
    
    public:

        BinaryFile(std::string const& filename)
            : filename(filename)
        {}

        void getEncoding() override {
            std::cout << "encoding utf-8" << std::endl;
        }

        virtual ~BinaryFile() {

        }     
};