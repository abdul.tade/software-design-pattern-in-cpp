# include <iostream>
# include <mutex>
# include <thread>
# include <chrono>
# include <queue>
# include <csignal>
# include "../include/LoggingService/LogMessage.hpp"
# include "../include/ConsumerProducerPattern/Consumer.hpp"
# include "../include/ConsumerProducerPattern/Producer.hpp"
# include "../include/ConsumerProducerPattern/Product.hpp"
# include "../include/ConsumerProducerPattern/ThreadSafeQueue.hpp"

void sighandler(int signum);
std::fstream file;
std::size_t transactionCount = 0;
std::chrono::system_clock::time_point start;

ThreadSafeQueue<Product> productQueue{};
Consumer<Product> consumer{"Bjarne Stroustroup",productQueue,messageQueue};
Producer<Product> producer{std::string("Abdul Hameed"),productQueue,messageQueue};

int main() 
{
    
    file.open("/home/deitybounce/.db_file",std::ios::binary);
    start = std::chrono::high_resolution_clock::now();
    
    auto l_future  = logger();
    auto c_future  = consumer();
	auto p_future  = producer(0);

    return 0;
}

void sighandler(int signum) {

    auto stop = std::chrono::system_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Application duration: " 
              << duration.count() 
              << " ms" << std::endl;
    
}