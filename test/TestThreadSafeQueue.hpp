# pragma once
# include <thread>
# include "../include/catch.hpp"
# include "../include/ConsumerProducerPattern/ThreadSafeQueue.hpp"

TEST_CASE("Adding element to queue","[ThreadSafeQueue<>::enqueue()]") {
    ThreadSafeQueue<int> queue{};

    queue.enqueue(100);
    queue.enqueue(-133);

    REQUIRE(queue.size() == 2);
    REQUIRE(queue.dequeue() == 100);
    REQUIRE(queue.dequeue() == -133);

    REQUIRE_THROWS_AS(queue.dequeue(),EmptyQueueException);
}

TEST_CASE("Removing element an element from queue","[ThreadSafeQueue<>::dequeue()]") {
    ThreadSafeQueue<int> queue{};

    queue.enqueue(10);
    queue.enqueue(-84);

    REQUIRE(queue.size() == 2);
    REQUIRE(queue.dequeue() == 100);
    REQUIRE(queue.dequeue() == -84);

    REQUIRE_THROWS_AS(queue.dequeue(),EmptyQueueException);
}

TEST_CASE("Size of a queue","[ThreadSafeQueue<>::size()]") {
    ThreadSafeQueue<int> queue{};

    queue.enqueue(10);
    queue.enqueue(12);

    REQUIRE(queue.size() == 2);

    queue.dequeue();
    queue.dequeue();

    REQUIRE(queue.size() == 0);
}

TEST_CASE("Queue is empty","[ThreadSafeQueue<>::is_empty()]") {
    ThreadSafeQueue<int> queue{};

    REQUIRE(queue.is_empty());
    
    queue.enqueue(10);
    queue.enqueue(-221);

    REQUIRE(!queue.is_empty());
}

TEST_CASE("Queue is thread safe","[]") {

    ThreadSafeQueue<int> queue_one{};

    std::jthread t1{};
}
