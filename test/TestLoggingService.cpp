# pragma once

# include <iostream>
# include <fstream>
# include <filesystem>
# include "../include/catch.hpp"
# include "../include/ConsumerProducerPattern/LoggingService.hpp"

std::ofstream& openFileStream(std::string const& path) {
    std::filesystem::path p{path};

    if(!std::filesystem::exists(p)) {
        throw std::runtime_error("File does not exist");
    }

    std::ofstream file;
    file.open(path,std::ios::app);
    return file;
}

std::size_t getFileSize(std::string const& path) {
    std::filesystem::path p{path};

    if(!std::filesystem::exists(p)) {
        return (std::size_t)-1;
    }

    return std::filesystem::file_size(p);
}

std::vector<std::string> readLine(std::istream& stream) {
    std::vector<std::string> lines;
    std::string line;

    while (std::getline(stream,line,'\n')) {
        lines.push_back(line);
    }

    return lines;
}

TEST_CASE("Logging service writing to file","LogginService::log()") {
    std::string filename = "/home/deitybounce/.db_file";
    std::ofstream& stream = openFileStream(filename);
    LoggingService logger{stream};

    for (std::size_t i = 0; i < 11; i++) {
        logger.log("Abdul Hameed");
    }

    stream.close();

    auto size = getFileSize(filename);
    REQUIRE(size != (std::size_t)-1);

    auto buffer = new char[size];
    REQUIRE(buffer != nullptr);

    std::ifstream file;
    file.open(filename,std::ios::binary);
    auto lines = readLine(file);

    for (auto const& line : lines) {
        REQUIRE(line == "Abdul Hameed");
    }

}