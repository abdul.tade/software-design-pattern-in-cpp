# pragma once
# include "../include/catch.hpp"
# include "../include/LoggingService/LogMessage.hpp"
# include "../include/ConsumerProducerPattern/Producer.hpp"
# include "../include/ConsumerProducerPattern/ThreadSafeQueue.hpp"

TEST_CASE("","") {
    
    ThreadSafeQueue<int> product_queue;
    ThreadSafeQueue<LogMessage> message_queue;

    Producer<int> producer("James Gosling",product_queue,message_queue);

}